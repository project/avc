Awin Voucher Codes

INTRODUCTION
------------

This module will import the voucher codes from the Awin network.

INSTALLATION
------------

Install as usual, see
Download the module and upload to the modules folder and make it active.

How to use
------------

There are three steps to follow.

1- Get registered with awin network.
2- Find your feed url. Save the feed url.
3- Run Cron. The module will check automatically new voucher codes with every cron run.
That's it.


MAINTAINERS
-----------

Current maintainers:

 * Naseem Sarwar (http://www.naseemsarwar.me)
 